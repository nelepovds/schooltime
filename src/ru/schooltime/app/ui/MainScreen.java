package ru.schooltime.app.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainScreen {
    private JPanel contentView;
    private JButton buttonConfirm;
    private JTextField textFieldCode;

    public MainScreen() {
        buttonConfirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (MainScreen.checkCurrentCode(textFieldCode.getText())) {
                    JOptionPane.showMessageDialog(contentView, "Работа за компьютером разрешена", "Все правильно", JOptionPane.INFORMATION_MESSAGE);
                    System.exit(0);
                } else {
                    JOptionPane.showMessageDialog(contentView, "Неправильный код", "Ошибка", JOptionPane.ERROR_MESSAGE);
                    textFieldCode.setText("");
                    textFieldCode.requestFocus();
                }
            }
        });
    }

    private static boolean checkCurrentCode(String text) {
        boolean canWork = false;

        String targetCode = generateCode();

        if (text.equalsIgnoreCase("27111984")
                || text.equalsIgnoreCase("25081987")
                || text.equalsIgnoreCase(targetCode.toString())) {
            canWork = true;
        }
        return canWork;
    }

    public static String generateCode() {
        Date currentDate = new Date();
        SimpleDateFormat day = new SimpleDateFormat("dd");
        Integer dayInt = Integer.parseInt(day.format(currentDate));

        String alphabet = "QWERTYUIOPASDFGHJKLZXCVBNM";
        SimpleDateFormat checkDate = new SimpleDateFormat("ddMMyyyy");
        String checkDateString = checkDate.format(currentDate);

        StringBuilder targetCode = new StringBuilder(checkDateString);

        Boolean evenDay = (dayInt % 2 == 0);
        for (int i = 0; i < checkDateString.length(); i++) {
            String currentDigit = checkDateString.substring(i, i + 1);
            Integer digit = Integer.parseInt(currentDigit);
            if ((i % 2 == 0) && evenDay) {
                digit *= 2;
            }
            String replaceString = alphabet.substring(digit, digit + 1);
            targetCode.replace(i, i + 1, replaceString);
        }
        return targetCode.toString();
    }

    public static void showMainScreen() {
        JFrame mainScreen = new JFrame("School Time");
        mainScreen.setContentPane(new MainScreen().contentView);
        mainScreen.setAlwaysOnTop(true);

        Dimension DimMax = Toolkit.getDefaultToolkit().getScreenSize();
        mainScreen.setMaximumSize(DimMax);
        mainScreen.setExtendedState(JFrame.MAXIMIZED_BOTH);

        mainScreen.setResizable(false);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        mainScreen.setSize(screenSize.width, screenSize.height);
        mainScreen.setUndecorated(true);

        mainScreen.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        mainScreen.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {
            }
        });

        mainScreen.pack();
        mainScreen.setVisible(true);

    }
}
